#include <string.h>
#include <stm32f7xx_hal.h>
#include <fmc_sdram.h>
#include <s6d0154x.h>


#define IF_SPI                  0
#define IF_RGB                  1

#define IF                      IF_RGB

#define SPI_BUFFERED_WRITE

#ifdef SPI_BUFFERED_WRITE
#define SPI_BUFF_SIZE           256
volatile uint16_t SPI_Buff[SPI_BUFF_SIZE] @ "SDRAMSEC";
#endif


/*
BOARD: StarterKit SK-STM32F746

//==============================================================================
����������� ������� �� SPI
//==============================================================================
STM32   SIGNAL          LCD PIN

PB5	MOSI		MOSI	
PA4	NSS             CS						
PA5	SCK 		SCK
PI9	Button		-
PH9	Backlight	BL

//==============================================================================
����������� ������� �� SPI+RGB
//==============================================================================
STM32   SIGNAL          LCD PIN

PB0	LTDC_R3		D00		
PH10	LTDC_R4		D01		
PH11	LTDC_R5		D02		
PB1	LTDC_R6		D03 	
PG6	LTDC_R7		D04		
PH13	LTDC_G2		D05		
PH14	LTDC_G3		D06		
PH15	LTDC_G4		D07		
PI0	LTDC_G5		D08		
PI1	LTDC_G6		D09		
PD3	LTDC_G7		D10		
PG11	LTDC_B3		D11		
PI4	LTDC_B4		D12		
PA3	LTDC_B5		D13		
PB8	LTDC_B6		D14		
PB9	LTDC_B7		D15
PG7	LTDC_CLK        PCLK		
PF10	LTDC_DE		DE
PI10	LTDC_HSYNC	HSYNC	
PI9	LTDC_VSYNC	VSYNC	
PB5	MOSI		MOSI	
PA4	NSS             CS						
PA5	SCK 		SCK
PI9	Button		-
PH9	Backlight	BL
*/



#define StartByteCommand        0x70
#define StartByteData           0x72

#define REG_DriverOutput        0x01
#define REG_DrivingWaveform     0x02
#define REG_EntryMode           0x03
#define REG_DisplayControl      0x07
#define REG_BlankPeriod         0x08
#define REG_FrameCycle          0x0B
#define REG_ExtDisplayIF        0x0C
#define REG_OscStart            0x0F
#define REG_PC_1                0x10
#define REG_PC_2                0x11
#define REG_PC_3                0x12
#define REG_PC_4                0x13
#define REG_PC_5                0x14
#define REG_VCI_Recycling       0x15
#define REG_Y_Set               0x20
#define REG_X_Set               0x21
#define CMD_WriteToGRAM         0x22
#define REG_GateScanPosition    0x30
#define REG_Partial_X_End       0x34
#define REG_Partial_X_Start     0x35
#define REG_Horiz_End           0x36
#define REG_Horiz_Start         0x37
#define REG_Vert_End            0x38
#define REG_Vert_Start          0x39

static uint16_t Width;
static uint16_t Height;
SPI_HandleTypeDef hspi1;
TIM_HandleTypeDef htim12;

#define LCD_CS_Port             GPIOA
#define LCD_CS_Pin              GPIO_PIN_4
#define LCD_CS_HIGH()           HAL_GPIO_WritePin(LCD_CS_Port, LCD_CS_Pin, GPIO_PIN_SET)
#define LCD_CS_LOW()            HAL_GPIO_WritePin(LCD_CS_Port, LCD_CS_Pin, GPIO_PIN_RESET)

#define LCD_RESET_Port          GPIOI
#define LCD_RESET_Pin           GPIO_PIN_5
#define LCD_RESET_HIGH()        HAL_GPIO_WritePin(LCD_RESET_Port, LCD_RESET_Pin, GPIO_PIN_SET)
#define LCD_RESET_LOW()         HAL_GPIO_WritePin(LCD_RESET_Port, LCD_RESET_Pin, GPIO_PIN_RESET)


//==============================================================================
// SPI1 Initialization Function
//==============================================================================
static void LCD_SPI1_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  // ������������� ����� ChipSelect
  GPIO_InitStruct.Pin = LCD_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(LCD_CS_Port, &GPIO_InitStruct);
  LCD_CS_HIGH();

  // SPI1 parameter configuration
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  // SPI_BAUDRATEPRESCALER_4;   // 25 ���
  // SPI_BAUDRATEPRESCALER_8;   // 12.5 ���
  // SPI_BAUDRATEPRESCALER_16;  // 6.25 ���
  // SPI_BAUDRATEPRESCALER_32;  // 3.125 ���
  // SPI_BAUDRATEPRESCALER_64;  // 1.5625 ���
  // SPI_BAUDRATEPRESCALER_128  // 0.78125 ���
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;

  HAL_SPI_Init(&hspi1);
}
//==============================================================================




#if (IF == IF_RGB)

// LTDC parameters
#define DISPLAY_WIDTH   240
#define DISPLAY_HEIGHT  320
#define DISPLAY_HSYNC   2
#define DISPLAY_HBP     6 
#define DISPLAY_HFP     2
#define DISPLAY_VSYNC   2
#define DISPLAY_VBP     6
#define DISPLAY_VFP     2

#define LOGO_WIDTH      200
#define LOGO_HEIGHT     20

// Frame Buffer
volatile uint16_t Layer1[DISPLAY_WIDTH * DISPLAY_HEIGHT] @ "SDRAMSEC";
// Logo Buffer
volatile uint16_t LogoBuff[LOGO_WIDTH * LOGO_HEIGHT] @ "SDRAMSEC";

LTDC_HandleTypeDef hltdc;
DMA2D_HandleTypeDef hdma2d;


// LTDC Initialization Function
int8_t LCD_LTDC_Init(void)
{
  hltdc.Instance = LTDC;
  hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
  hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
  hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
  hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IIPC;
  hltdc.Init.HorizontalSync = DISPLAY_HSYNC - 1;
  hltdc.Init.VerticalSync = DISPLAY_VSYNC - 1;
  hltdc.Init.AccumulatedHBP = DISPLAY_HSYNC + DISPLAY_HBP - 1;
  hltdc.Init.AccumulatedVBP = DISPLAY_VSYNC + DISPLAY_VBP - 1;
  hltdc.Init.AccumulatedActiveW = DISPLAY_WIDTH + DISPLAY_HSYNC + DISPLAY_HBP - 1;
  hltdc.Init.AccumulatedActiveH = DISPLAY_HEIGHT + DISPLAY_VSYNC + DISPLAY_VBP - 1;
  hltdc.Init.TotalWidth = DISPLAY_WIDTH + DISPLAY_HSYNC + DISPLAY_HBP + DISPLAY_HFP - 1; 
  hltdc.Init.TotalHeigh = DISPLAY_HEIGHT + DISPLAY_VSYNC + DISPLAY_VBP + DISPLAY_VFP - 1;
  hltdc.Init.Backcolor.Blue = 0x00;
  hltdc.Init.Backcolor.Green = 0xFF;
  hltdc.Init.Backcolor.Red = 0x00;

  if (HAL_LTDC_Init(&hltdc) != HAL_OK)
    return -1;

  memset((void *) Layer1, 0, sizeof(Layer1));
  
  LTDC_LayerCfgTypeDef pLayerCfg = {0};
  pLayerCfg.WindowX0 = 0;
  pLayerCfg.WindowX1 = DISPLAY_WIDTH;
  pLayerCfg.WindowY0 = 0;
  pLayerCfg.WindowY1 = DISPLAY_HEIGHT;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB565;
  pLayerCfg.Alpha = 255;
  pLayerCfg.Alpha0 = 0;
  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_CA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_CA;
  pLayerCfg.FBStartAdress = (uint32_t) &Layer1;
  pLayerCfg.ImageWidth = DISPLAY_WIDTH;
  pLayerCfg.ImageHeight = DISPLAY_HEIGHT;
  pLayerCfg.Backcolor.Blue = 0x00;
  pLayerCfg.Backcolor.Green = 0x00;
  pLayerCfg.Backcolor.Red = 0x00;

  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK)
    return -1;

  return 0;
}


// DMA2D Initialization Function
int8_t MX_DMA2D_Init(void)
{
  /*
  hdma2d.Instance = DMA2D;
  hdma2d.Init.Mode = DMA2D_M2M;
  hdma2d.Init.ColorMode = DMA2D_OUTPUT_RGB888;
  hdma2d.Init.OutputOffset = 0;
  hdma2d.LayerCfg[1].InputOffset = 0;
  hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_RGB888;
  hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d.LayerCfg[1].InputAlpha = 0;

  if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
  {
    return -1;
  }

  if (HAL_DMA2D_ConfigLayer(&hdma2d, 1) != HAL_OK)
  {
    return -1;
  }
*/
  return 0;
}

#endif

//==============================================================================
//
//==============================================================================
static void SPI_WriteComm(uint16_t cmd)
{
  uint32_t timeout = 100;
  uint8_t buff[] = {StartByteCommand, 0, cmd};
  
  LCD_CS_LOW();
  HAL_SPI_Transmit(&hspi1, buff, 3, timeout);
  LCD_CS_HIGH();
}
//==============================================================================


//==============================================================================
//
//==============================================================================
static void SPI_WriteData(uint16_t data)
{			
  uint32_t timeout = 100;
  uint8_t buff[] = {StartByteData, data >> 8, data};

  LCD_CS_LOW();
  HAL_SPI_Transmit(&hspi1, buff, 3, timeout);
  LCD_CS_HIGH();
}
//==============================================================================


//==============================================================================
//
//==============================================================================
static void SPI_WR_REG(uint16_t Index, uint16_t CongfigTemp)
{
  SPI_WriteComm(Index);
  SPI_WriteData(CongfigTemp);
}
//==============================================================================


#if (IF == IF_SPI)
//==============================================================================
//
//==============================================================================
static void BlockWrite(unsigned int Xstart,unsigned int Xend,unsigned int Ystart,unsigned int Yend) 
{
  SPI_WR_REG(REG_Y_Set, Ystart);
  SPI_WR_REG(REG_X_Set, Xstart);
	
  SPI_WR_REG(REG_Horiz_End, Yend);
  SPI_WR_REG(REG_Horiz_Start, Ystart);	
  SPI_WR_REG(REG_Vert_End, Xend);
  SPI_WR_REG(REG_Vert_Start, Xstart);

  SPI_WriteComm(CMD_WriteToGRAM);
}
//==============================================================================
#endif


//==============================================================================
// ��������� ���������� �������������� ������ color
//==============================================================================
void s6d0154x_FillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
#if (IF == IF_SPI)

  uint32_t timeout = 100;

  BlockWrite(x, x + w - 1, y, y + h - 1);

  LCD_CS_LOW();

  uint8_t buff[] = {0x72 , 0x00};
  HAL_SPI_Transmit(&hspi1, buff, 1, timeout);

#ifdef SPI_BUFFERED_WRITE
  // ������ � ������� � ������������
  uint32_t total_len = w * h;
  
  while (total_len)
  {
    uint32_t len = total_len > SPI_BUFF_SIZE ? SPI_BUFF_SIZE : total_len;
    for (uint32_t i = 0; i < len; i++)
      SPI_Buff[i] = (color << 8) | (color >> 8);
    
    HAL_SPI_Transmit(&hspi1, (void *)SPI_Buff, len * 2, timeout);
    total_len -= len;
  }
#else
  //
  buff[0] = color >> 8;
  buff[1] = color;
  
  for (int i = 0; i < w * h; i++)
    HAL_SPI_Transmit(&hspi1, buff, 2, timeout);
#endif
  LCD_CS_HIGH();

#elif (IF == IF_RGB)

  for (uint32_t y_idx = y; y_idx < y + h; y_idx++)
    for (uint32_t x_idx = x; x_idx < x + w; x_idx++)
      s6d0154x_DrawPixel(x_idx, y_idx, color);

#endif
}
//==============================================================================


//==============================================================================
// ���������� 1 ������ ������ ����
//==============================================================================
void s6d0154x_LoadLogoPart(uint16_t y, uint16_t *pBuff, uint16_t w, uint16_t h)
{
#if (IF == IF_RGB)
  for (uint32_t x = 0; x < w; x++)
    LogoBuff[x * h + y] = *(pBuff++);
#endif
}
//==============================================================================


//==============================================================================
// ����� ���� �� 2 ���� �� ��������� ����������� � �������� �������������
//==============================================================================
void s6d0154x_ShowLogo(uint16_t x, uint16_t y, uint8_t alpha)
{
#if (IF == IF_RGB)
  LTDC_LayerCfgTypeDef pLayerCfg = {0};
  pLayerCfg.WindowX0 = y;
  pLayerCfg.WindowX1 = y + LOGO_HEIGHT;
  pLayerCfg.WindowY0 = x;
  pLayerCfg.WindowY1 = x + LOGO_WIDTH;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB565;
  pLayerCfg.Alpha = alpha;
  pLayerCfg.Alpha0 = 0;
  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  pLayerCfg.FBStartAdress = (uint32_t) &LogoBuff;
  pLayerCfg.ImageWidth = LOGO_HEIGHT;
  pLayerCfg.ImageHeight = LOGO_WIDTH;
  pLayerCfg.Backcolor.Blue = 0x00;
  pLayerCfg.Backcolor.Green = 0x00;
  pLayerCfg.Backcolor.Red = 0x00;

  HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 1);
#endif
}
//==============================================================================


//==============================================================================
//
//==============================================================================
void s6d0154x_DrawPart(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t *pBuff)
{
#if (IF == IF_SPI)
  uint32_t timeout = 100;

  BlockWrite(x, x + w - 1, y, y + h - 1);

  LCD_CS_LOW();

  uint8_t buff[] = {0x72 , 0x00};
  HAL_SPI_Transmit(&hspi1, buff, 1, timeout);

#ifdef SPI_BUFFERED_WRITE
  // ������ � ������� � ������������
  uint32_t total_len = w * h;
  
  while (total_len)
  {
    uint32_t len = total_len > SPI_BUFF_SIZE ? SPI_BUFF_SIZE : total_len;
    for (uint32_t i = 0; i < len; i++)
    {
      SPI_Buff[i] = (*pBuff << 8) | (*pBuff >> 8);
      pBuff++;
    }
    
    HAL_SPI_Transmit(&hspi1, (void *)SPI_Buff, len * 2, timeout);
    total_len -= len;
  }
#else
  //
  for (int i = 0; i < w * h; i++)
  {
    buff[0] = *pBuff >> 8;
    buff[1] = *pBuff;
    HAL_SPI_Transmit(&hspi1, buff, 2, timeout);
    pBuff++;
  }
#endif


  LCD_CS_HIGH();
#elif (IF == IF_RGB)
  
  for (uint32_t y_idx = y; y_idx < y + h; y_idx++)
    for (uint32_t x_idx = x; x_idx < x + w; x_idx++)
      s6d0154x_DrawPixel(x_idx, y_idx, *(pBuff++));

#endif
}
//==============================================================================


//==============================================================================
// ��������� ���������� 1 ������� �������
//==============================================================================
void s6d0154x_DrawPixel(int16_t x, int16_t y, uint16_t color)
{
  if ((x < 0) || (x >= Width) || (y < 0) || (y >= Height))
    return;
  
#if (IF == IF_SPI)
  SPI_WR_REG(REG_Y_Set, y);
  SPI_WR_REG(REG_X_Set, x);
  SPI_WriteComm(CMD_WriteToGRAM);
  SPI_WriteData(color);
#elif (IF == IF_RGB)
  Layer1[x * DISPLAY_WIDTH + y] = color;
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������� �������
//==============================================================================
void s6d0154x_SetBL(uint8_t value)
{
  __HAL_TIM_SET_COMPARE(&htim12, TIM_CHANNEL_2, value);
}
//==============================================================================


// 1 ���
void s6d0154x_BackLight_init()
{
  __HAL_RCC_TIM12_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  htim12.Instance = TIM12;
  htim12.Init.Prescaler = 1000;
  htim12.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim12.Init.Period = 99;
  htim12.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim12.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  HAL_TIM_PWM_Init(&htim12);

  TIM_OC_InitTypeDef sConfigOC;
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  HAL_TIM_PWM_ConfigChannel(&htim12, &sConfigOC, TIM_CHANNEL_2);

  // TIM12 GPIO Configuration    
  // PH9     ------> TIM12_CH2 
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF9_TIM12;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
  
  HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_2);

  s6d0154x_SetBL(0);
}

//==============================================================================
// ��������� �������������� ������� �������
//==============================================================================
void s6d0154x_init(uint16_t width, uint16_t height)
{
  Width = width;
  Height = height;

  // ������������� ����� ���������� ����������
  s6d0154x_BackLight_init();
  
  // ������������� RESET
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = LCD_RESET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LCD_RESET_Port, &GPIO_InitStruct);

  // ������������� SPI1 ��� ���������� ��������
  LCD_SPI1_Init();

  LCD_RESET_LOW();
  HAL_Delay(120);
  LCD_RESET_HIGH();
  HAL_Delay(120);

  //************************S6D0154+LPL2.6************************
  SPI_WR_REG(REG_PC_2, 0x001C);  // Power Control 2
  SPI_WR_REG(REG_PC_3, 0x1212);  // Power Control 3  BT2-0, DC11-10, DC21-20, DC31-30
  SPI_WR_REG(REG_PC_4, 0x0041);  // Power Control 4  DCR_EX=0, DCR2-0, GVD6-0
  SPI_WR_REG(REG_PC_5, 0x526A);  // VCOMH = GVDD*8525  // was 0x4050 VCOMH = GVDD*0.7535);  // Power Control 5  VCOMG=0, VCM6-0, VCMR=0, VML6-0
  SPI_WR_REG(REG_PC_1, 0x0C00);  // Fast1 // was 0x0800);  // MediumFast1// Power Control 1 
  HAL_Delay(10);
					
  SPI_WR_REG(REG_PC_2, 0x011C); // PON=1
  HAL_Delay(10);
  SPI_WR_REG(REG_PC_2, 0x031C); // PON1=1
  HAL_Delay(10);
  SPI_WR_REG(REG_PC_2, 0x071C); // PON2=1
  HAL_Delay(10);
  SPI_WR_REG(REG_PC_2, 0x0F1C); // PON3=1
  HAL_Delay(10);
  SPI_WR_REG(REG_PC_2, 0x0F3C); // AON=1
  HAL_Delay(10);

  // Other mode set
  SPI_WR_REG(REG_DriverOutput, 0x1028); 
  SPI_WR_REG(REG_DrivingWaveform, 0x0100);
  
  SPI_WR_REG(REG_BlankPeriod, 0x0808); 
  SPI_WR_REG(REG_FrameCycle, 0x1105); //  <= 72Hz

#if (IF == IF_SPI)
  SPI_WR_REG(REG_EntryMode, 0x1030);
  SPI_WR_REG(REG_ExtDisplayIF, 0x0002); // SPI/MPU 
#else
  SPI_WR_REG(REG_EntryMode, 0x0030);
  SPI_WR_REG(REG_ExtDisplayIF, 0x0101); // 16-bit RGB, RGB  //was 0x0002); // 6-bit RGB 
#endif

  SPI_WR_REG(0x000E, 0x0200); 
  SPI_WR_REG(REG_OscStart, 0x1801); 
  SPI_WR_REG(REG_VCI_Recycling, 0x0020);  // <= EQ Mode : Vcom ??

  SPI_WR_REG(REG_Y_Set, 0x0000);
  SPI_WR_REG(REG_X_Set, 0x0000);
  HAL_Delay(5);
  
  SPI_WR_REG(REG_GateScanPosition, 0x0000);
  SPI_WR_REG(REG_Partial_X_End, 0x013f);
  SPI_WR_REG(REG_Partial_X_Start, 0x0000);

  HAL_Delay(5);
  SPI_WR_REG(REG_Horiz_End, 0x00ef);
  SPI_WR_REG(REG_Horiz_Start, 0x0000);	
  SPI_WR_REG(REG_Vert_End, 0x013f);
  SPI_WR_REG(REG_Vert_Start, 0x0000);
  HAL_Delay(5);

  // Gamma set	
  SPI_WR_REG(0x0050, 0x0101); 
  SPI_WR_REG(0x0051, 0x0903); 
  SPI_WR_REG(0x0052, 0x0e0e); 
  SPI_WR_REG(0x0053, 0x0001); 
  SPI_WR_REG(0x0054, 0x0200); 
  SPI_WR_REG(0x0055, 0x0809); 
  SPI_WR_REG(0x0056, 0x0e0e); 
  SPI_WR_REG(0x0057, 0x0100); 
  SPI_WR_REG(0x0058, 0x0606); 
  SPI_WR_REG(0x0059, 0x0100);
  SPI_WR_REG(REG_OscStart, 0x1e01);   
  
  // Display on sequence
  SPI_WR_REG(REG_DisplayControl, 0x0013);		// D1-0=11
  HAL_Delay(10);
  SPI_WriteComm(CMD_WriteToGRAM);

#if (IF == IF_RGB)
  // ������������� SDRAM
  MX_FMC_Init();
  // ������������� LTDC (RGB)
  LCD_LTDC_Init();
  // ������������� DMA2D
  //MX_DMA2D_Init();
#endif
  
  s6d0154x_SetBL(100);
}
//==============================================================================
