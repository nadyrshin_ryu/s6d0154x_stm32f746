#include "stm32f7xx_hal.h"
#include "fmc_sdram.h"


SDRAM_HandleTypeDef hsdram1;

static FMC_SDRAM_CommandTypeDef Command;
#define SDRAM_TIMEOUT     ((uint32_t)0xFFFF)

#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)


// Programs the SDRAM device.
int8_t BSP_SDRAM_Initialization_sequence(SDRAM_HandleTypeDef *pSdramHandle, uint32_t RefreshCount)
{
  __IO uint32_t tmpmrd = 0;
  
  // Step 1: Configure a clock configuration enable command
  Command.CommandMode            = FMC_SDRAM_CMD_CLK_ENABLE;
  Command.CommandTarget          = (1 << 4);//FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber      = 1;
  Command.ModeRegisterDefinition = 0;
  // Send the command
  HAL_SDRAM_SendCommand(pSdramHandle, &Command, SDRAM_TIMEOUT);

  // Step 2: Insert 100 us minimum delay 
  HAL_Delay(1);
    
  // Step 3: Configure a PALL (precharge all) command  
  Command.CommandMode            = FMC_SDRAM_CMD_PALL;
  Command.CommandTarget          = FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber      = 1;
  Command.ModeRegisterDefinition = 0;
  // Send the command
  HAL_SDRAM_SendCommand(pSdramHandle, &Command, SDRAM_TIMEOUT);  
  
  // Step 4: Configure an Auto Refresh command 
  Command.CommandMode            = FMC_SDRAM_CMD_AUTOREFRESH_MODE;
  Command.CommandTarget          = FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber      = 8;
  Command.ModeRegisterDefinition = 0;
  // Send the command
  HAL_SDRAM_SendCommand(pSdramHandle, &Command, SDRAM_TIMEOUT);

  // Step 5: Program the external memory mode register
  tmpmrd = (uint32_t)SDRAM_MODEREG_BURST_LENGTH_1          |\
                     SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL   |\
                     SDRAM_MODEREG_CAS_LATENCY_3           |\
                     SDRAM_MODEREG_OPERATING_MODE_STANDARD |\
                     SDRAM_MODEREG_WRITEBURST_MODE_SINGLE;
  
  Command.CommandMode            = FMC_SDRAM_CMD_LOAD_MODE;
  Command.CommandTarget          = FMC_SDRAM_CMD_TARGET_BANK1;
  Command.AutoRefreshNumber      = 1;
  Command.ModeRegisterDefinition = tmpmrd;
  // Send the command 
  HAL_SDRAM_SendCommand(pSdramHandle, &Command, SDRAM_TIMEOUT);
  
  // Step 6: Set the refresh rate counter
  HAL_SDRAM_ProgramRefreshRate(pSdramHandle, RefreshCount); 

  return 0;
}

//#define SDRAM_DEVICE_ADDR  ((uint32_t)0xC0000000)
//uint8_t *pSDRAM = (uint8_t *) SDRAM_DEVICE_ADDR;
#define REFRESH_COUNT                    ((uint32_t)0x0603)   /* SDRAM refresh counter (100Mhz SD clock) */


void MPU_RegionConfig(void)
{
   MPU_Region_InitTypeDef MPU_InitStruct;
   
   /* ������ MPU */
   HAL_MPU_Disable();

   /* ���������������� ������� RAM ��� Region 0, �������� 8 ��������, � ������������ ������ � ������ (R/W) */
   MPU_InitStruct.Enable = MPU_REGION_ENABLE;
   MPU_InitStruct.BaseAddress = 0x60000000;
   MPU_InitStruct.Size = MPU_REGION_SIZE_32MB;
   MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
   MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
   MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
   MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
   MPU_InitStruct.Number = MPU_REGION_NUMBER0;
   MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
   MPU_InitStruct.SubRegionDisable = 0x00;
   MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;
   HAL_MPU_ConfigRegion(&MPU_InitStruct);

   /* ���������� ������ MPU */
   HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

// FMC initialization function
int8_t MX_FMC_Init(void)
{
  FMC_SDRAM_TimingTypeDef SdramTiming = {0};

  HAL_EnableFMCMemorySwapping();
  MPU_RegionConfig();

  /** Perform the SDRAM1 memory initialization sequence */
  hsdram1.Instance = FMC_SDRAM_DEVICE;
  /* hsdram1.Init */
  hsdram1.Init.SDBank = FMC_SDRAM_BANK1;
  hsdram1.Init.ColumnBitsNumber = FMC_SDRAM_COLUMN_BITS_NUM_9;
  hsdram1.Init.RowBitsNumber = FMC_SDRAM_ROW_BITS_NUM_13;
  hsdram1.Init.MemoryDataWidth = FMC_SDRAM_MEM_BUS_WIDTH_16;
  hsdram1.Init.InternalBankNumber = FMC_SDRAM_INTERN_BANKS_NUM_4;
  hsdram1.Init.CASLatency = FMC_SDRAM_CAS_LATENCY_3;//2;
  hsdram1.Init.WriteProtection = FMC_SDRAM_WRITE_PROTECTION_DISABLE;
  hsdram1.Init.SDClockPeriod = FMC_SDRAM_CLOCK_PERIOD_2;
  hsdram1.Init.ReadBurst = FMC_SDRAM_RBURST_ENABLE;//FMC_SDRAM_RBURST_DISABLE;
  hsdram1.Init.ReadPipeDelay = FMC_SDRAM_RPIPE_DELAY_0;
  
/* SdramTiming */
  SdramTiming.LoadToActiveDelay    = 2;
  SdramTiming.ExitSelfRefreshDelay = 7;
  SdramTiming.SelfRefreshTime      = 4;
  SdramTiming.RowCycleDelay        = 7;
  SdramTiming.WriteRecoveryTime    = 2;
  SdramTiming.RPDelay              = 2;
  SdramTiming.RCDDelay             = 2;

  if (HAL_SDRAM_Init(&hsdram1, &SdramTiming) != HAL_OK)
  {
    return -1;
  }

  // SDRAM initialization sequence
  return BSP_SDRAM_Initialization_sequence(&hsdram1, REFRESH_COUNT);
}
