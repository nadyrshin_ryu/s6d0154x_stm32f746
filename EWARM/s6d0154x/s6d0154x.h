#ifndef __S6D0154X_H
#define __S6D0154X_H

#include <stdint.h>


// ��������� �������������� ������� �������
void s6d0154x_init(uint16_t Width, uint16_t Height);
// ��������� ���������� 1 ������� �������
void s6d0154x_DrawPixel(int16_t x, int16_t y, uint16_t color);
// ��������� ���������� �������������� ������ color
void s6d0154x_FillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
//
void s6d0154x_DrawPart(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t *pBuff);
// ��������� ������� �������
void s6d0154x_SetBL(uint8_t Value);
// ���������� 1 ������ ������ ����
void s6d0154x_LoadLogoPart(uint16_t y, uint16_t *pBuff, uint16_t w, uint16_t h);
// ����� ���� �� 2 ���� �� ��������� ����������� � �������� �������������
void s6d0154x_ShowLogo(uint16_t x, uint16_t y, uint8_t alpha);


#endif /* __S6D0154X_H */
