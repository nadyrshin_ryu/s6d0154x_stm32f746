#ifndef __FMC_SDRAM_H
#define __FMC_SDRAM_H

#include <stdint.h>


// FMC initialization function
int8_t MX_FMC_Init(void);

#endif /* __SDRAM_H */
