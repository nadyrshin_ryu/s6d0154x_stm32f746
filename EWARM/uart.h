#ifndef __UART_H
#define __UART_H

#include <stdint.h>


int8_t MX_USART1_UART_Init(void);
int8_t MX_USART2_UART_Init(void);
int8_t MX_USART6_UART_Init(void);


#endif /* __UART_H */
