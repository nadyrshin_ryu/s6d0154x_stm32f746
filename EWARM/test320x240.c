//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "main.h"
#include <string.h>
#include <dispcolor.h>
#include <font.h>
#include <ff.h>
#include <diskio.h>
#include <button.h>
#include <s6d0154x.h>

#define DELAY_MODE      0
#define BUTTON_MODE     1
#define MODE            BUTTON_MODE

#if (MODE == BUTTON_MODE)
#define TEST_Delay(a)   button_WaitPress()
#else
#define TEST_Delay(a)   HAL_Delay(a)
#endif



//==============================================================================
// 
//==============================================================================
void Test_LoadLogo(const char* fname)
{
  FATFS fatfs;
  FRESULT res;
  
  res = f_mount(&fatfs, "0", 1);
  if (res != FR_OK)
    return;
  
  FIL file;
  
  res = f_open(&file, fname, FA_READ);
  if (res != FR_OK)
    return;

  unsigned int bytesRead;
  uint8_t header[34];
  res = f_read(&file, header, sizeof(header), &bytesRead);
  if (res != FR_OK) 
  {
    f_close(&file);
    return;
  }

  if ((header[0] != 0x42) || (header[1] != 0x4D))
  {
    f_close(&file);
    return;
  }

  uint32_t imageOffset = header[10] | (header[11] << 8) | (header[12] << 16) | (header[13] << 24);
  uint32_t imageWidth  = header[18] | (header[19] << 8) | (header[20] << 16) | (header[21] << 24);
  uint32_t imageHeight = header[22] | (header[23] << 8) | (header[24] << 16) | (header[25] << 24);
  uint16_t imagePlanes = header[26] | (header[27] << 8);

  uint16_t imageBitsPerPixel = header[28] | (header[29] << 8);
  uint32_t imageCompression  = header[30] | (header[31] << 8) | (header[32] << 16) | (header[33] << 24);

  if((imagePlanes != 1) || (imageBitsPerPixel != 24) || (imageCompression != 0))
  {
    f_close(&file);
    return;
  }

  res = f_lseek(&file, imageOffset);
  if(res != FR_OK)
  {
    f_close(&file);
    return;
  }

  // �������������� ����� ������ �������� ��� ������
  uint8_t imageRow[(200 * 3 + 3) & ~3];
  uint16_t PixBuff[200];

  for (uint32_t y = 0; y < imageHeight; y++)
  {
    res = f_read(&file, imageRow, (imageWidth * 3 + 3) & ~3, &bytesRead);
    if (res != FR_OK)
    {
      f_close(&file);
      return;
    }
      
    uint32_t rowIdx = 0;
    for (uint32_t x = 0; x < imageWidth; x++)
    {
      uint8_t b = imageRow[rowIdx++];
      uint8_t g = imageRow[rowIdx++];
      uint8_t r = imageRow[rowIdx++];
      PixBuff[x] = RGB565(r, g, b);
    }

    s6d0154x_LoadLogoPart(imageHeight - y - 1, PixBuff, imageWidth, imageHeight);
  }

  f_close(&file);
}
//==============================================================================


//==============================================================================
// 
//==============================================================================
void Test_ShowLogo(uint16_t x, uint16_t y, uint8_t alpha)
{
  s6d0154x_ShowLogo(x, y, alpha);
}
//==============================================================================


//==============================================================================
// ���� ������� �����  �� ��������� ����������� � ������ ����� ���������
//==============================================================================
void Test_TextFading(char *pStr, int16_t X, int16_t Y)
{
  char Str[32];
  uint8_t Len = strlen(pStr);
  
  dispcolor_ClearScreen();
  dispcolor_SetBrightness(100);
  
  // ��������� �������
  for (uint8_t i = 0; i < Len; i++)
  {
    memcpy(Str, pStr, i + 1);
    Str[i + 1] = 0;
    dispcolor_printf(X, Y, FONTID_16F, RGB565(255, 255, 255), Str);
    HAL_Delay(10);
  }
  // ��������� LOGO
  for (uint16_t i = 0; i <= 255; i++)
  {
    Test_ShowLogo(60, 220, i);
    HAL_Delay(5);
  }
  
  HAL_Delay(1000);
  
  // ������� ������������ �������
  for (uint8_t i = 0; i <= 100; i++)
  {
    dispcolor_SetBrightness(100 - i);
    HAL_Delay(15);
  }
  
  dispcolor_ClearScreen();
  HAL_Delay(500);
  
  Test_ShowLogo(60, 220, 128);
  dispcolor_SetBrightness(100);
}
//==============================================================================



//==============================================================================
// ���� �������� ������� ������� �������
//==============================================================================
void Test_Colors(void)
{
  dispcolor_ClearScreen();
  dispcolor_SetBrightness(100);

  uint16_t color = RGB565(255, 0, 0);
  dispcolor_FillScreen(color);
  TEST_Delay(1000);
  
  color = RGB565(0, 255, 0);
  dispcolor_FillScreen(color);
  TEST_Delay(1000);

  color = RGB565(0, 0, 255);
  dispcolor_FillScreen(color);
  TEST_Delay(1000);

  color = RGB565(255, 255, 0);
  dispcolor_FillScreen(color);
  TEST_Delay(1000);

  color = RGB565(255, 0, 255);
  dispcolor_FillScreen(color);
  TEST_Delay(1000);

  color = RGB565(0, 255, 255);
  dispcolor_FillScreen(color);
  TEST_Delay(1000);

  color = RGB565(255, 255, 255);
  dispcolor_FillScreen(color);
  TEST_Delay(1000);

  dispcolor_ClearScreen();
  HAL_Delay(500);
}
//==============================================================================

//==============================================================================
// ���� ��������� ������� �� ������� �������� � SD-������
//==============================================================================
void Test_displayImage(const char* fname)
{
  FRESULT res;
  
  FIL file;
  res = f_open(&file, fname, FA_READ);
  if (res != FR_OK)
    return;

  unsigned int bytesRead;
  uint8_t header[34];
  res = f_read(&file, header, sizeof(header), &bytesRead);
  if (res != FR_OK) 
  {
    f_close(&file);
    return;
  }

  if ((header[0] != 0x42) || (header[1] != 0x4D))
  {
    f_close(&file);
    return;
  }

  uint32_t imageOffset = header[10] | (header[11] << 8) | (header[12] << 16) | (header[13] << 24);
  uint32_t imageWidth  = header[18] | (header[19] << 8) | (header[20] << 16) | (header[21] << 24);
  uint32_t imageHeight = header[22] | (header[23] << 8) | (header[24] << 16) | (header[25] << 24);
  uint16_t imagePlanes = header[26] | (header[27] << 8);

  uint16_t imageBitsPerPixel = header[28] | (header[29] << 8);
  uint32_t imageCompression  = header[30] | (header[31] << 8) | (header[32] << 16) | (header[33] << 24);

  if ((imagePlanes != 1) || (imageBitsPerPixel != 24) || (imageCompression != 0))
  {
    f_close(&file);
    return;
  }

  res = f_lseek(&file, imageOffset);
  if (res != FR_OK)
  {
    f_close(&file);
    return;
  }

  // �������������� ����� ������ �������� ��� ������
  uint8_t imageRow[(320 * 3 + 3) & ~3];
  uint16_t PixBuff[320];

  for (uint32_t y = 0; y < imageHeight; y++)
  {
    res = f_read(&file, imageRow, (imageWidth * 3 + 3) & ~3, &bytesRead);
    if (res != FR_OK)
    {
      f_close(&file);
      return;
    }
      
    uint32_t rowIdx = 0;
    for (uint32_t x = 0; x < imageWidth; x++)
    {
      uint8_t b = imageRow[rowIdx++];
      uint8_t g = imageRow[rowIdx++];
      uint8_t r = imageRow[rowIdx++];
      PixBuff[x] = RGB565(r, g, b);
    }

    dispcolor_DrawPartXY(0, imageHeight - y - 1, imageWidth, 1, PixBuff);
  }

  f_close(&file);
}
//==============================================================================


//==============================================================================
// ���� ������ �� ������� ������ ������� ��������
//==============================================================================
void Test320x240_Text(void)
{
  dispcolor_ClearScreen();
  dispcolor_SetBrightness(100);
  
  dispcolor_printf(0, 0, FONTID_6X8M, RGB565(255, 255, 255), "������ ������ ������ ������������ ������� 6x8");
  dispcolor_printf(0, 8, FONTID_6X8M, RGB565(255, 200, 200), "���������� ������� 320x240");
  dispcolor_printf(0, 16, FONTID_6X8M, RGB565(200, 255, 200), "��������� 61 �� / 2.4\"");
  dispcolor_printf(0, 24, FONTID_6X8M, RGB565(200, 200, 255), "������� ������� ������� 49 x 36.5 ��");
  dispcolor_printf(0, 32, FONTID_6X8M, RGB565(255, 255, 200), "��������� �������� 166.67 PPI");
  dispcolor_printf(0, 40, FONTID_6X8M, RGB565(200, 255, 255), "���������� ������� Samsung S6D0154X");
  dispcolor_printf(0, 48, FONTID_6X8M, RGB565(255, 200, 255), "16-������ ���� (RGB565)");
  dispcolor_printf(0, 56, FONTID_6X8M, RGB565(255, 255, 255), "����������� � ���������");

  dispcolor_printf(0, 72, FONTID_6X8M, RGB565(255, 255, 255), "������ ������ ������ ������������ ������� 6x8");
  dispcolor_printf(0, 80, FONTID_6X8M, RGB565(255, 200, 200), "���������� ������� 320x240");
  dispcolor_printf(0, 88, FONTID_6X8M, RGB565(200, 255, 200), "��������� 61 �� / 2.4\"");
  dispcolor_printf(0, 96, FONTID_6X8M, RGB565(200, 200, 255), "������� ������� ������� 49 x 36.5 ��");
  dispcolor_printf(0, 104, FONTID_6X8M, RGB565(255, 255, 200), "��������� �������� 166.67 PPI");
  dispcolor_printf(0, 112, FONTID_6X8M, RGB565(200, 255, 255), "���������� ������� Samsung S6D0154X");
  dispcolor_printf(0, 120, FONTID_6X8M, RGB565(255, 200, 255), "16-������ ���� (RGB565)");
  dispcolor_printf(0, 128, FONTID_6X8M, RGB565(255, 255, 255), "����������� � ���������");

  dispcolor_printf(0, 144, FONTID_6X8M, RGB565(255, 255, 255), "������ ������ ������ ������������ ������� 6x8");
  dispcolor_printf(0, 152, FONTID_6X8M, RGB565(255, 200, 200), "���������� ������� 320x240");
  dispcolor_printf(0, 160, FONTID_6X8M, RGB565(200, 255, 200), "��������� 61 �� / 2.4\"");
  dispcolor_printf(0, 168, FONTID_6X8M, RGB565(200, 200, 255), "������� ������� ������� 49 x 36.5 ��");
  dispcolor_printf(0, 176, FONTID_6X8M, RGB565(255, 255, 200), "��������� �������� 166.67 PPI");
  dispcolor_printf(0, 184, FONTID_6X8M, RGB565(200, 255, 255), "���������� ������� Samsung S6D0154X");
  dispcolor_printf(0, 192, FONTID_6X8M, RGB565(255, 200, 255), "16-������ ���� (RGB565)");
  dispcolor_printf(0, 200, FONTID_6X8M, RGB565(255, 255, 255), "����������� � ���������");
  
  dispcolor_printf(0, 216, FONTID_6X8M, RGB565(255, 255, 255), "������ ������ ������ ������������ ������� 6x8");
  dispcolor_printf(0, 224, FONTID_6X8M, RGB565(255, 200, 200), "���������� ������� 320x240");
  dispcolor_printf(0, 232, FONTID_6X8M, RGB565(200, 255, 200), "��������� 61 �� / 2.4\"");

  TEST_Delay(2000);
  dispcolor_ClearScreen();
  
  dispcolor_printf(0, 0, FONTID_16F, RGB565(255, 255, 255), "������ ������ ������ �������������-\r\n��� ������� ������� 16p");
  dispcolor_printf(0, 32, FONTID_16F, RGB565(255, 200, 200), "���������� ������� 320x240");
  dispcolor_printf(0, 48, FONTID_16F, RGB565(200, 255, 200), "��������� 61 �� / 2.4\"");
  dispcolor_printf(0, 64, FONTID_16F, RGB565(200, 200, 255), "������� ������� ������� 49x36.5 ��");
  dispcolor_printf(0, 80, FONTID_16F, RGB565(255, 255, 200), "��������� �������� 166.67 PPI");
  dispcolor_printf(0, 96, FONTID_16F, RGB565(200, 255, 255), "���������� ������� Samsung S6D0154X");
  dispcolor_printf(0, 112, FONTID_16F, RGB565(255, 200, 255), "16-������ ���� (RGB565)");
  dispcolor_printf(0, 128, FONTID_16F, RGB565(255, 255, 255), "����������� � ���������");

  dispcolor_printf(0, 160, FONTID_16F, RGB565(255, 255, 255), "������ ������ ������ �������������-\r\n��� ������� ������� 16p");
  dispcolor_printf(0, 192, FONTID_16F, RGB565(255, 200, 200), "���������� ������� 320x240");
  dispcolor_printf(0, 208, FONTID_16F, RGB565(200, 255, 200), "��������� 61 �� / 2.4\"");
  dispcolor_printf(0, 224, FONTID_16F, RGB565(200, 200, 255), "������� ������� ������� 49x36.5 ��");

  TEST_Delay(2000);
  dispcolor_ClearScreen();
  
  dispcolor_printf(0, 0, FONTID_24F, RGB565(255, 255, 255), "012345678901234567890123456");
  dispcolor_printf(0, 24, FONTID_24F, RGB565(255, 200, 200), "012345678901234567890123456");
  dispcolor_printf(0, 48, FONTID_24F, RGB565(200, 255, 200), "012345678901234567890123456");
  dispcolor_printf(0, 72, FONTID_24F, RGB565(200, 200, 255), "012345678901234567890123456");
  dispcolor_printf(0, 96, FONTID_24F, RGB565(255, 255, 200), "012345678901234567890123456");
  dispcolor_printf(0, 120, FONTID_24F, RGB565(255, 200, 255), "012345678901234567890123456");
  dispcolor_printf(0, 144, FONTID_24F, RGB565(200, 255, 255), "012345678901234567890123456");
  dispcolor_printf(0, 168, FONTID_24F, RGB565(255, 255, 255), "012345678901234567890123456");
  dispcolor_printf(0, 192, FONTID_24F, RGB565(255, 200, 200), "012345678901234567890123456");
  dispcolor_printf(0, 216, FONTID_24F, RGB565(200, 255, 200), "012345678901234567890123456");

  TEST_Delay(2000);
  dispcolor_ClearScreen();
  
  dispcolor_printf(0, 0, FONTID_32F, RGB565(255, 255, 255), "0123456789012345678901");
  dispcolor_printf(0, 32, FONTID_32F, RGB565(255, 200, 200), "0123456789012345678901");
  dispcolor_printf(0, 64, FONTID_32F, RGB565(200, 255, 200), "0123456789012345678901");
  dispcolor_printf(0, 96, FONTID_32F, RGB565(200, 200, 255), "0123456789012345678901");
  dispcolor_printf(0, 128, FONTID_32F, RGB565(255, 255, 200), "0123456789012345678901");
  dispcolor_printf(0, 160, FONTID_32F, RGB565(255, 200, 255), "0123456789012345678901");
  dispcolor_printf(0, 192, FONTID_32F, RGB565(200, 255, 255), "0123456789012345678901");
  dispcolor_printf(0, 224, FONTID_32F, RGB565(255, 255, 255), "0123456789012345678901");

  TEST_Delay(2000);
  dispcolor_ClearScreen();
  HAL_Delay(500);
  
  dispcolor_SetBrightness(100);
}
//==============================================================================


//==============================================================================
// ���� ������ ������� 2D-������� �� �������
//==============================================================================
void Test320x240_Graphics(void)
{
  dispcolor_ClearScreen();
  dispcolor_SetBrightness(100);
  
  // �����
  dispcolor_DrawRectangle(0, 0, 159, 79, RGB565(255, 255, 255));
  dispcolor_DrawLine(130, 79, 159, 50, RGB565(255, 255, 255));
  
  HAL_Delay(500);

  // ��������
  dispcolor_DrawRectangleFilled(0, 0, 26, 79, RGB565(200, 200, 200));
  dispcolor_DrawRectangle(0, 0, 26, 26, RGB565(255, 255, 255));
  dispcolor_DrawRectangle(0, 26, 26, 52, RGB565(255, 255, 255));
  dispcolor_DrawRectangle(0, 52, 26, 79, RGB565(255, 255, 255));
  HAL_Delay(200);
  dispcolor_DrawCircleFilled(13, 13, 12, RGB565(255, 0, 0));
  dispcolor_DrawCircle(13, 13, 12, RGB565(255, 255, 255));
  dispcolor_DrawCircleFilled(13, 39, 12, RGB565(255, 200, 0));
  dispcolor_DrawCircle(13, 39, 12, RGB565(255, 255, 255));
  dispcolor_DrawCircleFilled(13, 65, 12, RGB565(0, 220, 0));
  dispcolor_DrawCircle(13, 65, 12, RGB565(255, 255, 255));

  HAL_Delay(200);

  // ��� ��������������
  dispcolor_DrawRectangleFilled(30, 5, 115, 74, RGB565(50, 50, 255));
  dispcolor_DrawRectangle(30, 5, 115, 74, RGB565(255, 255, 255));
  dispcolor_DrawRectangleFilled(120, 5, 155, 40, RGB565(50, 255, 50));
  dispcolor_DrawRectangle(120, 5, 155, 40, RGB565(255, 255, 255));
  dispcolor_DrawRectangleFilled(100, 20, 140, 60, RGB565(255, 50, 50));
  dispcolor_DrawRectangle(100, 20, 140, 60, RGB565(255, 255, 255));
  dispcolor_DrawRectangleFilled(110, 30, 130, 50, RGB565(255, 255, 0));
  
  HAL_Delay(200);
  
  // ��� �����
  dispcolor_DrawCircleFilled(75, 47, 30, RGB565(200, 50, 255));
  dispcolor_DrawCircle(75, 47, 30, RGB565(255, 255, 255));
  dispcolor_DrawCircleFilled(65, 40, 10, RGB565(255, 255, 255));
  
  TEST_Delay(2000);

  dispcolor_ClearScreen();
  HAL_Delay(500);
  
  dispcolor_SetBrightness(100);
}
//==============================================================================


//==============================================================================
// ���� ������ �������� �� �������
//==============================================================================
void Test320x240_Images(void)
{
  FATFS fatfs;
  DIR DirInfo;
  FILINFO FileInfo;
  FRESULT res;
  
  res = f_mount(&fatfs, "0", 1);
  if (res != FR_OK)
    return;
  
  res = f_chdir("/320x240");
  if (res != FR_OK)
    return;

  res = f_opendir(&DirInfo, "");
  if (res != FR_OK)
    return;
  
  while (1)
  {
    res = f_readdir(&DirInfo, &FileInfo);
    if (res != FR_OK)
      break;
      
    if (FileInfo.fname[0] == 0)
      break;
      
    char *pExt = strstr(FileInfo.fname, ".BMP");
    if (pExt)
    {
      Test_displayImage(FileInfo.fname);
      HAL_Delay(2000);
    }
  }
}
//==============================================================================
