/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include <stm32f7xx_hal.h>
#include <stm32f7xx_hal_sd.h>
#include <main.h>
#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */

extern SD_HandleTypeDef hsd1;


/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
// pdrv         Physical drive nmuber to identify the drive
/*-----------------------------------------------------------------------*/
DSTATUS disk_status(BYTE pdrv)
{
  HAL_SD_CardStatusTypeDef cardStatus;
  HAL_StatusTypeDef result = HAL_SD_GetCardStatus(&hsd1, &cardStatus);

  if (result)
    return STA_NOINIT;
  else
    return 0;
}


/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/* pdrv - Physical drive nmuber to identify the drive */
/*-----------------------------------------------------------------------*/
DSTATUS disk_initialize(BYTE pdrv)
{
  uint8_t sd_state = 0;
  
  /* HAL SD initialization */
  sd_state = HAL_SD_Init(&hsd1);

  /* Configure SD Bus width */
  if (sd_state == 0)
  {
    /* Enable wide operation */
    if (HAL_SD_ConfigWideBusOperation(&hsd1, SDMMC_BUS_WIDE_4B) != 0)
      sd_state = STA_NOINIT;
    else
      sd_state = 0;
  }

  return sd_state;
}


/*-----------------------------------------------------------------------*/
// Read Sector(s)                                                        
// pdrv         Physical drive nmuber to identify the drive 
// *buff	Data buffer to store read data 
// sector       Start sector in LBA 
// count	Number of sectors to read 
/*-----------------------------------------------------------------------*/
DRESULT disk_read(BYTE pdrv, BYTE *buff, DWORD sector, UINT count)
{
  uint8_t sd_state;

  if (pdrv || !count)
    return RES_PARERR;
  
  if (HAL_SD_ReadBlocks(&hsd1, buff, sector, count, 100) != 0)
    sd_state = STA_NOINIT;
  else
    sd_state = 0;

  return sd_state;  
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
  /*
	DRESULT res;
	int result;

	switch (pdrv) {
	case DEV_RAM :
		// translate the arguments here

		result = RAM_disk_write(buff, sector, count);

		// translate the reslut code here

		return res;

	case DEV_MMC :
		// translate the arguments here

		result = MMC_disk_write(buff, sector, count);

		// translate the reslut code here

		return res;

	case DEV_USB :
		// translate the arguments here

		result = USB_disk_write(buff, sector, count);

		// translate the reslut code here

		return res;
	}
*/
	return RES_PARERR;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
  /*
	DRESULT res;
	int result;

	switch (pdrv) {
	case DEV_RAM :

		// Process of the command for the RAM drive

		return res;

	case DEV_MMC :

		// Process of the command for the MMC/SD card

		return res;

	case DEV_USB :

		// Process of the command the USB drive

		return res;
	}
*/
	return RES_PARERR;
}

