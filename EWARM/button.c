//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "stm32f7xx_hal.h"
#include <button.h>


//==============================================================================
// ��������� ������������� ���� GPIO ��� ����������� ������
//==============================================================================
void button_Init()
{
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = TestButton_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(TestButton_Port, &GPIO_InitStruct);
}
//==============================================================================


//==============================================================================
// ������� ������ ��������� ������
//==============================================================================
uint8_t button_GetState(void)
{
  return HAL_GPIO_ReadPin(TestButton_Port, TestButton_Pin);
}
//==============================================================================


//==============================================================================
// ��������� �������� ������� (� ����������) ������
//==============================================================================
void button_WaitPress(void)
{
  // ��� ������� ������ ����� ����������� �������
  while (!button_GetState())
    HAL_Delay(100);
  
  // ��� ������� ������
  while (button_GetState())
    HAL_Delay(100);
}
//==============================================================================
