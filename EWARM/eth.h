#ifndef __ETH_H
#define __ETH_H

#include <stdint.h>


// FMC initialization function
int8_t MX_ETH_Init(void);

#endif /* __ETH_H */
